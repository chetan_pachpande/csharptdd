﻿using System;

namespace FizzBuzz.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 100; i++)
            {
                System.Console.WriteLine(i);
            }

            System.Console.Read();
        }
    }
}

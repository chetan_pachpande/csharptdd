﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace FizzBuzz.Library.Test
{
    [TestFixture]
    public class FizzBuzzerTest
    {
        [Test]
        public void Buzzer_WhenDiv3_ReturnsFizz([Values(3, 6)] int input)
        {            
            string result = FizzBuzzer.GetValue(input);
            Assert.AreEqual("Fizz", result);
        }

        [Test]
        public void Buzzer_WhenDiv5_ReturnsBuzz([Values(5, 10)] int input)
        {           
            string result = FizzBuzzer.GetValue(input);
            Assert.AreEqual("Buzz", result);
        }

        [Test]
        public void Buzzer_WhenDiv3AndDiv5_ReturnsFizzBuzz([Values(15, 30)] int input)
        {
            string result = FizzBuzzer.GetValue(input);
            Assert.AreEqual("FizzBuzz", result);
        }

        [Test]
        public void Buzzer_WhenDefault_ReturnsInput_v1([Values(1, 2, 4)] int input)
        {
            string result = FizzBuzzer.GetValue(input);
            Assert.AreEqual(input.ToString(), result);
        }

        [Test]
        public void Buzzer_WhenDefault_ReturnsInput_v2()
        {
            int input = GetRandomInput();
            string result = FizzBuzzer.GetValue(input);
            Assert.AreEqual(input.ToString(), result);
        }

        #region Private Methods

        private int GetRandomInput()
        {
            var intList = new List<int>();
            for (int i = 1; i <= 100; i++)
            {
                if ((i % 3 == 0) || (i % 5 == 0))
                    continue;
                intList.Add(i);
            }
            var rnd = new Random();
            var randIndex = rnd.Next(intList.Count);
            return intList[randIndex];
        }

        #endregion
    }
}

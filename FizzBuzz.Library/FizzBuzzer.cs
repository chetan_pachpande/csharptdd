﻿using System;

namespace FizzBuzz.Library
{

    /// <summary>
    /// if a number is divisible by,
    /// 3 > print "Fizz"
    /// 5 > print "Buzz"
    /// 3 and 5 > print "FizzBuzz"
    /// other > print the number
    /// </summary>
    public class FizzBuzzer
    {
        public static string GetValue(int input)
        {
            if (input % 3 == 0 && input % 5 == 0)
                return "FizzBuzz";
            if (input % 3 == 0)
                return "Fizz";
            if (input % 5 == 0)
                return "Buzz";
            return input.ToString();
        }
    }
}
